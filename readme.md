# C++ OOP Program 1

**Student:** Liam Hockley

**Submitted Date:** Jan 23, 2012 3:40 pm

**Grade:** 30.0 (max 30.0)

**Instructions**
> Develop a class called Student with the following operations.  
> 
> This class will provide a user program to enter student FullName, Age, and Gender.  It > will also list all these values if needed by a user program.
> 
> The class Student will contain three private data members - FullName, Age, and Gender.
> It will contain the following methods (functions)
> - a constructor that initializes all private members to zero or null string
> - setFullName that takes a string parameter (Name) but returns void.  It sets the private >member FullName to the argument Name.
> -setAge takes an int (a) but returns void.  It sets the private member Age to the > argument a.
> -setGender takes a char (g) returns void.  It sets the private member Gender to the >argument g.
> 
> -getFullName - no parameters but returns a string (FullName).
> -getAge - no parameters but returns an int (Age).
> -getGender - no parameters but returns a char (Gender);
> 
> Then define those functions in Student.cpp and develop a main.cpp program that creates three objects and tests all the methods > of the class.
