// Students.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{
	Student s1;
	Student s2;
	Student s3;
	
	std::cout<<"[STUDENT 1]------------------------------------\n";
	s1.SetValues("Liam",'M',25);
	std::cout<<"Student Name:"  <<s1.GetName()		<<std::endl;
	std::cout<<"Student Gender:"<<s1.GetGender()	<<std::endl;
	std::cout<<"Student Age:"   <<s1.GetAge()		<<std::endl; 
	std::cout<<"------------------------------------------------\n";

	std::cout<<"[STUDENT 2]------------------------------------\n";
	s2.SetValues("John",'M',20);
	std::cout<<"Student Name:"  <<s2.GetName()		<<std::endl;
	std::cout<<"Student Gender:"<<s2.GetGender()	<<std::endl;
	std::cout<<"Student Age:"   <<s2.GetAge()		<<std::endl; 
	std::cout<<"------------------------------------------------\n";

	std::cout<<"[STUDENT 3]------------------------------------\n";
	s3.SetValues("Tayler",'F',18);
	std::cout<<"Student Name:"  <<s3.GetName()		<<std::endl;
	std::cout<<"Student Gender:"<<s3.GetGender()	<<std::endl;
	std::cout<<"Student Age:"   <<s3.GetAge()		<<std::endl; 
	std::cout<<"------------------------------------------------\n";

	std::cin>>std::ws;
	return 0;
}

