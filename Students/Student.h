#ifndef STUDENT_H
#define STUDENT_H

//declare Student Class prototype
class Student
{
public: //declare public members/methods
	Student(void);
	void SetValues (std::string, char, int);
	std::string GetName ();
	char GetGender ();
	int GetAge ();

private://declare private data members
	std::string st_name;
	char st_gender;
	int st_age;
};

#endif